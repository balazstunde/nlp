import xml.etree.ElementTree as etree
import os
from datetime import datetime
import re
p = re.compile('#REDIRECT \[\[(.*)\]\]')

now = datetime.now()

current_time = now.strftime("%H:%M:%S")
print("Start Time =", current_time)

PATH_WIKI_XML = 'D:\\mesteri\\sem2\\nlp'
FILENAME_WIKI = 'enwiki-20061130-pages-articles.xml'

root = etree.Element("wiki")

file1 = open('wikiexample.xml', 'w', encoding="UTF-8") 


def strip_tag_name(t):
    # t = elem.tag
    idx = k = t.rfind("}")
    if idx != -1:
        t = t[idx + 1:]
    return t


pathWikiXML = os.path.join(PATH_WIKI_XML, FILENAME_WIKI)

totalCount = 0
title = ""
text = ""



for event, elem in etree.iterparse(pathWikiXML, events=('start', 'end')):
    tname = strip_tag_name(elem.tag)
    # print(tname)

    if event == 'start':
        if tname == 'page':
            title = ''
            id = -1
            redirect = ''
            inrevision = False
            ns = 0
        elif tname == 'revision':
            # Do not pick up on revision id's
            inrevision = True
    else:
        if tname == 'text':
            text = elem.text
            #print(text)
        elif tname == 'title':
            title = elem.text
        #elif tname == 'redirect':
        #    title = elem.attrib['title']
        #    print("redirect")
        elif tname == 'page':
            totalCount += 1

            if text != None:
                title_redirect = p.findall(text)
                if title_redirect != []:
                    title = title_redirect[0]
            pa = etree.SubElement(root, "page")
            etree.SubElement(pa, "title").text = title
            etree.SubElement(pa, "text").text = text

            if totalCount > 100000:
                break

            if totalCount > 1 and (totalCount % 100000) == 0:
                print("{:,}".format(totalCount))

        elem.clear()

tree = etree.ElementTree(root)
tree.write("littlewikidump.xml")
now = datetime.now()
current_time = now.strftime("%H:%M:%S")
print("End Time =", current_time)
